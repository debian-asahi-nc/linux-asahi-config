#!/usr/bin/env python3

import re
import sys
import time

argv = sys.argv[1:]

if argv[0] == '--m-is-y':
    m_is_y = True
    argv = [argv[1], argv[2]]
else:
    m_is_y = False

with open(argv[0]) as fin:
    f1 = [l.strip() for l in fin]

with open(argv[1]) as fin:
    f2 = [l.strip() for l in fin]

for l in f2:
    if l == '':
        continue
    c = re.search(r'CONFIG_(?P<CONFIG>[^\s=]*)[\s=]', l)
    if c is None:
        continue
    c = c.group('CONFIG')
    found = False
    for ll in f1:
        if c in ll:
            c2 = re.search(r'CONFIG_(?P<CONFIG>[^\s=]*)[\s=]', ll)
            if c2 is None:
                continue
            if c == c2.group('CONFIG'):
                found = True
                if m_is_y:
                    l = l.replace('=m', '=y')
                    ll = ll.replace('=m', '=y')
                if l != ll:
                    print(l)
                    sys.stdout.flush()
                    print(f'  OLD: {ll}', file=sys.stderr)
                break
    if not found:
        print(l, '')
        sys.stdout.flush()
        print(f'  OLD: ABSENT', file=sys.stderr)
